from django.shortcuts import render, redirect, reverse
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse, HttpResponseRedirect
import json

from ..models import Chatt, Profile

import re as regex


def index(request):
    if request.user.is_authenticated():
        return redirect('home')
    else:
        return render(request, 'chatter/core/index.html')


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')

    form = UserCreationForm()
    return render(request, 'chatter/core/signup.html', {'form': form})


def search(request):
    def on_error():
        return redirect('home')

    search_input = request.POST.get('search_input')[:30]
    if len(search_input) == 0:
        return on_error()
    if not regex.match(r'^[a-zA-Z0-9_@\+\.\-]+$', search_input):
        return on_error()
    return HttpResponseRedirect(reverse('search_page', args=(search_input,)))


def search_page(request, search_input):
    profiles = Profile.objects.all().filter(user__username__startswith=search_input)
    return render(
        request,
        'chatter/core/search_page.html',
        {
            'authenticated': request.user.is_authenticated(),
            'search_input': search_input,
            'user_profiles': profiles,
        }
    )

def newest_chatt(request):
    if not request.user.is_authenticated():
        return HttpResponse('Available only for authenticated users.')
 
    chatts = Chatt.objects.filter(author=request.user.profile).order_by('pub_date')
    if not chatts:
        return HttpResponse('You didn\'t add any chatts')

    data = {
        'username': str(request.user),
        'content': chatts[len(chatts) - 1].content
    }
    return HttpResponse(json.dumps(data))
