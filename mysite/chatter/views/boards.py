from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone

from ..models import Chatt, Profile


def __render__(request, *args, **context):
    if request.user.is_authenticated():
        context['authenticated'] = True

        if request.method == 'POST':
            author = request.user.profile
            content = request.POST.get('chatt_content')[:200]
            if len(content) == 0:
                context['error_message'] = "You can't post an empty Chatt."
                return render(request, *args, context=context)
            chatt = Chatt(author=author, content=content, pub_date=timezone.now())
            chatt.save()
            return redirect('home')

    return render(request, *args, context=context)


def home(request):
    if not request.user.is_authenticated():
        return redirect('index')
    return __render__(request, 'chatter/boards/home.html', entries=Chatt.objects.all())


def user(request, username):
    profile = get_object_or_404(Profile, user__username=username)
    entries = profile.chatt_set.all()
    return __render__(
        request,
        'chatter/boards/user_board.html',
        entries=entries,
        profile=username,
    )
