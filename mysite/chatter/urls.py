from django.conf.urls import url
from django.contrib.auth import views as auth_views

from .views import core as core_views
from .views import boards as board_views

urlpatterns = [
    url(r'^$', core_views.index, name='index'),
    url(r'login/', auth_views.login, {'template_name': 'chatter/core/login.html'}, name='login'),
    url(r'logout/', auth_views.logout, {'next_page': 'index'}, name='logout'),
    url(r'signup/', core_views.signup, name='signup'),
    url(r'^home/', board_views.home, name='home'),
    url(r'^profile/(?P<username>[a-zA-Z0-9_@\+\.\-]+)/$', board_views.user, name='user_profile'),
    url(r'^search/$', core_views.search, name='search'),
    url(r'^search_page/(?P<search_input>[a-zA-Z0-9_@\+\.\-]+)/$', core_views.search_page, name='search_page'),
    url(r'newest_chatt/', core_views.newest_chatt)
]